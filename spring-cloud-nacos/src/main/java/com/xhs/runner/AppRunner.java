package com.xhs.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.net.InetAddress;

/**
 * @desc: 启动控制台打印信息
 * @projectName: spring-cloud-nacos
 * @author: xhs
 * @date: 2024-03-12 012 16:05
 * @version: JDK 1.8
 */
@Slf4j
@Component
public class AppRunner implements CommandLineRunner {
    @Value("${server.port}")
    private String port;
    @Value("${spring.application.name}")
    private String serviceName;
    @Value("${spring.profiles.active}")
    private String active;

    /**
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        InetAddress addr = InetAddress.getLocalHost();
        String ipAddr = addr.getHostAddress();
        log.info("--------------------当前启动环境为：{}--------------------",active);
        log.info("接口访问地址: http://{}:{}", ipAddr, port);
        log.info("接口访问地址: http://localhost:{}", port);
        log.info("--------------------{}服务启动成功--------------------",serviceName);
    }
}
