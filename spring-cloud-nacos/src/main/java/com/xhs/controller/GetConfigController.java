package com.xhs.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @desc: 读取nacos配置中心配置
 * @projectName: spring-cloud-nacos
 * @author: xhs
 * @date: 2024-03-08 008 14:19
 * @version: JDK 1.8
 */
// 刷新配置
@RefreshScope
@Slf4j
@RestController
public class GetConfigController {
    /**
     * 当前环境
     */
    @Value("${current}")
    private String current;
    /**
     * 客户端ID
     */
    @Value("${clientId}")
    private String clientId;
    /**
     * 公钥
     */
    @Value("${publicKey}")
    private String publicKey;
    /**
     * 用户名
     */
    @Value("${username}")
    private String username;
    /**
     * 密码
     */
    @Value("${password}")
    private String password;

    /**
     * 获取配置中心配置
     *
     * @return Map<String, Object>
     */
    @GetMapping("/getConfig")
    public Map<String, Object> getConfig() {
        Map<String, Object> map = new HashMap<>(16);
        map.put("current", current);
        map.put("clientId", clientId);
        map.put("publicKey", publicKey);
        map.put("username", username);
        map.put("password", password);
        log.debug("动态调整日志级别为debug");
        log.info("动态调整日志级别为info");
        return map;
    }
}
