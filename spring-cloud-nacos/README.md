# spring-cloud-nacos

### 介绍📖
spring-cloud整合nacos

### 使用技术📔

spring-boot 2.7.18

spring-cloud  2021.0.9

spring-cloud-starter-alibaba-nacos 2021.0.6.0

### 启动步骤📚

#### 1.启动nacos，nacos版本需要大于2.2.3

#### 2.创建nacos命名空间

![image-20240312162009890](https://gitee.com/xhs101/nacos-server/raw/master/img/image-20240312162009890.png)

#### 3.在创建的命名空间下创建配置文件

![image-20240312162122792](https://gitee.com/xhs101/nacos-server/raw/master/img/image-20240312162122792.png)

```yaml
# 当前环境
current: 开发环境
# 客户端ID
clientId: SHDWKD56435656fLKJL
# 公钥
publicKey: WSASDFGBCFOKC0986FMCJFD111
# 用户名
username: admin
# 密码
password: 123123
```



![image-20240312162207440](https://gitee.com/xhs101/nacos-server/raw/master/img/image-20240312162207440.png)

```yaml
# 日志配置
logging:
  # 日志配置文件
  config: classpath:logback-spring.xml
  file:
    # 日志生成的目录
    path: ./log
    # 日志文件名称
    log-name: app
    # 日志文件保留天数
    days: 3
  level:
    ROOT: info
    com.xhs: info
```



![image-20240312162301319](https://gitee.com/xhs101/nacos-server/raw/master/img/image-20240312162301319.png)

#### 4.修改代码的配置文件 bootstrap.yml

1.修改nacos地址、账号、密码

2.修改命名空间

![image-20240312162819574](https://gitee.com/xhs101/nacos-server/raw/master/img/image-20240312162819574.png)
