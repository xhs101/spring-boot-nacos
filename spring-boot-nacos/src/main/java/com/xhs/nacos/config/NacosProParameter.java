package com.xhs.nacos.config;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @desc: 读取nacos的配置
 * @projectName: spring-nacos-service
 * @author: xhs
 * @date: 2022/11/5 17:18
 * @version: JDK 1.8
 */
@Data
@Component
// 刷新配置
@NacosPropertySource(dataId = "constant.yaml", autoRefreshed = true)
public class NacosProParameter {
    /**
     * 当前环境
     */
    @NacosValue(value = "${current}", autoRefreshed = true)
    private String current;
    /**
     * 客户端ID
     */
    @NacosValue(value = "${clientId}", autoRefreshed = true)
    private String clientId;
    /**
     * 公钥
     */
    @NacosValue(value = "${publicKey}", autoRefreshed = true)
    private String publicKey;
    /**
     * 用户名
     */
    @NacosValue(value = "${username}", autoRefreshed = true)
    private String username;
    /**
     * 密码
     */
    @NacosValue(value = "${password}", autoRefreshed = true)
    private String password;
}
