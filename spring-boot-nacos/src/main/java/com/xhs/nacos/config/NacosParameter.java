package com.xhs.nacos.config;

import com.alibaba.nacos.api.config.ConfigType;
import com.alibaba.nacos.api.config.annotation.NacosConfigurationProperties;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @desc: 读取nacos的配置
 * @projectName: spring-nacos-service
 * @author: xhs
 * @date: 2022/11/5 17:18
 * @version: JDK 1.8
 */
@Data
@Component
// 刷新配置
@NacosConfigurationProperties(dataId = "constant.yaml",groupId = "DEFAULT_GROUP",type = ConfigType.YAML,autoRefreshed = true)
public class NacosParameter {
    /**
     * 当前环境
     */
    private String current;
    /**
     * 客户端ID
     */
    private String clientId;
    /**
     * 公钥
     */
    private String publicKey;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
}
