package com.xhs.nacos.controller;

import com.xhs.nacos.config.NacosParameter;
import com.xhs.nacos.config.NacosProParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @desc:
 * @projectName: spring-nacos-service
 * @author: xhs
 * @date: 2022/11/5 16:57
 * @version: JDK 1.8
 */
@RestController
public class GetNacosConfigController {

    @Autowired
    private NacosParameter nacosParameter;

    @Autowired
    private NacosProParameter nacosProParameter;

    /**
     * 获取naos配置
     *
     * @return
     */
    @GetMapping("/getConfig")
    public Map<String, Object> getConfig() {
        Map<String, Object> map = new HashMap<>(16);
        map.put("current", nacosParameter.getCurrent());
        map.put("clientId", nacosParameter.getClientId());
        map.put("publicKey", nacosParameter.getPublicKey());
        map.put("username", nacosParameter.getUsername());
        map.put("password", nacosParameter.getPassword());
        return map;
    }

    /**
     * 获取naos配置
     *
     * @return
     */
    @GetMapping("/getProConfig")
    public Map<String, Object> getProConfig() {
        Map<String, Object> map = new HashMap<>(16);
        map.put("current", nacosProParameter.getCurrent());
        map.put("clientId", nacosProParameter.getClientId());
        map.put("publicKey", nacosProParameter.getPublicKey());
        map.put("username", nacosProParameter.getUsername());
        map.put("password", nacosProParameter.getPassword());
        return map;
    }
}
