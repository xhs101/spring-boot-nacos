# spring-boot-nacos

### 介绍📖
spring-boot整合nacos

### 使用技术📔

spring-boot 2.7.18

nacos-discovery-spring-boot-starter 0.2.12

nacos-config-spring-boot-starter 0.2.12

### 启动步骤📚

#### 1.启动nacos，nacos版本需要大于2.2.3

#### 2.创建nacos命名空间

![image-20240312162009890](https://gitee.com/xhs101/nacos-server/raw/master/img/image-20240312162009890.png)

#### 3.在创建的命名空间下创建配置文件

![image-20240312162122792](https://gitee.com/xhs101/nacos-server/raw/master/img/image-20240312162122792.png)

```yaml
# 当前环境
current: 开发环境
# 客户端ID
clientId: SHDWKD56435656fLKJL
# 公钥
publicKey: WSASDFGBCFOKC0986FMCJFD111
# 用户名
username: admin
# 密码
password: 123123
```



![image-20240312162207440](https://gitee.com/xhs101/nacos-server/raw/master/img/image-20240312162207440.png)

#### 4.修改代码的配置文件

1.修改nacos地址、账号、密码

2.修改命名空间

![image-20240312165149766](https://gitee.com/xhs101/nacos-server/raw/master/img/image-20240312165149766.png)
