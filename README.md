# nacos-server

### 介绍📖
spring-boot、spring-cloud整合nacos动态获取配置文件的内容

### 项目结构📚
```text
nacos-server
├─ spring-boot-nacos       # spring-boot整合nacos
├─ spring-cloud-nacos      # spring-cloud整合nacos
```

